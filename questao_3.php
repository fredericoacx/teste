<?php

class Teste {
      
    public $num_1;
    public $num_2;
    public $num_3;

    public function getNum1() {
        return $this->num_1;
    }

    public function setNum1($valor) {
        $this->num_1 = $valor;
    }

    public function getNum2() {
        return $this->num_2;
    }

    public function setNum2($valor) {
        $this->num_2 = $valor;
    }

    public function getNum3() {
        return $this->num_3;
    }

    public function setNum3($valor) {
        $this->num_3 = $valor;
    }

    public function multiplica() {
        if($this->num_1 && $this->num_2 && $this->num_3) {
            return $this->num_1 * $this->num_2 * $this->num_3;
        }
    }    
}

$teste = new Teste();

$teste->setNum1(2);
$teste->setNum2(4);
$teste->setNum3(5);

echo $teste->multiplica();


